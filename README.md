# Docker image for Continuous Integration

## System information
  * Ubuntu 20.04

## Installed packages

For our needs, we install these packages : 

  * ssh
  * openssh-client
  * rsync
  * curl
  * wget
  * PHP 8.1
  * Composer 2
  * Node 14 & NPM 7

> Thanks to https://github.com/vyuldashev
